class ChatData {
  final String name;
  final String message;
  final String imageUrl;

  ChatData({this.name, this.message, this.imageUrl});

  factory ChatData.formJson(Map json){
    return ChatData(
      name:json['name'],
      message:json['message'],
      imageUrl:json['imageUrl'],
    );
  }
}

