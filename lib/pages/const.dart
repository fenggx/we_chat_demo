import 'package:flutter/material.dart';

final ThemeColor = Color.fromRGBO(237, 237, 237, 1.0);


//获取屏幕高度
double ScreenHeight(context){
  return MediaQuery.of(context).size.height;
}
//获取屏幕宽
double ScreenWidth(context){
  return MediaQuery.of(context).size.width;
}
