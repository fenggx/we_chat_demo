import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:we_chat_demo/pages/const.dart';
import 'package:we_chat_demo/pages/root_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.yellow,//主题色 深色状态栏标题为白色，反之黑色
        highlightColor: Color.fromRGBO(1, 0, 0, 0.0),
        splashColor: Color.fromRGBO(1, 0, 0, 0.0),
        cardColor: Color.fromRGBO(1, 1, 1, 0.6)
      ),
      home: RootPage(),
    );
  }
}

